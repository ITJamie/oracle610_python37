
FROM --platform=linux/amd64 oraclelinux:6.10
# COPY repos/base.repo /etc/yum.repos.d/CentOS-Base.repo
# Install yum dependencies
# Install yum dependencies
RUN yum -y update && \
    yum groupinstall -y development && \
    yum install -y \
    bzip2-devel \
    git \
    hostname \
    openssl \
    openssl-devel \
    sqlite-devel \
    sudo \
    tar \
    wget \
    zlib-dev \
    libffi \
    libffi-devel \
    which \
    tar

# SSL Fix https://benad.me/blog/2018/07/17/python-3.7-on-centos-6/
RUN cd /tmp && \
    wget --no-check-certificate 'https://www.openssl.org/source/openssl-1.1.0h.tar.gz' &&\
    tar -xf openssl-1.1.0h.tar.gz &&\
    cd openssl-1.1.0h &&\
    ./config shared --prefix=/usr/local/openssl11 --openssldir=/usr/local/openssl11 && make && make install
# End of SSL fix

# # Install python3.7
# RUN cd /tmp && \
#     wget https://www.python.org/ftp/python/3.7.13/Python-3.7.13.tgz && \
#     tar xvfz Python-3.7.13.tgz && \
#     cd Python-3.7.13 && \
#     # Fix the SSL headers https://benad.me/blog/2018/07/17/python-3.7-on-centos-6/
#     sed -i 's/SSL=\/usr\/local\/ssl/SSL=\/usr\/local\/openssl11/g' Modules/Setup.dist &&\
#     sed -i '211,214 s/^##*//' Modules/Setup.dist &&\
#     # End of SSL fix
#     LDFLAGS="-Wl,-rpath=/usr/local/openssl11/lib" \
#     ./configure --prefix=/usr/local --enable-shared --with-openssl=/usr/local/openssl11 --with-system-ffi && \
#     make && \
#     make install

# # Update libraries bindings
# RUN echo "/usr/local/lib" >> /etc/ld.so.conf
# RUN ldconfig
# # Update the default pip version shipped with python
# RUN pip3.7 install --upgrade pip
# run pip3 install virtualenv

# RUN git clone https://github.com/pyenv/pyenv.git ~/.pyenv
# RUN echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
# RUN echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile 
# RUN echo 'eval "$(pyenv init -)"' >> ~/.bash_profile

run cd /root; \
    wget https://github.com/conda-forge/miniforge/releases/download/4.8.3-5/Miniforge3-4.8.3-5-Linux-x86_64.sh -O miniforge3.sh; \
    chmod +x miniforge3.sh;\ 
    ./miniforge3.sh -b; \
    source /root/miniforge3/bin/activate; \
    conda init
